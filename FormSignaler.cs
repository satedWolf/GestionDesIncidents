﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionDesIncidents
{
    public partial class FormSignaler : Form
    {
        public FormSignaler()
        {
            InitializeComponent();
        }

        private void btnRetour_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormIncident form = new FormIncident();
            form.Cateorie = 1;
            form.Show();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormIncident form = new FormIncident();
            form.Cateorie = 2;
            form.Show();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FormIncident form = new FormIncident();
            form.Cateorie = 3;
            form.Show();
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FormIncident form = new FormIncident();
            form.Cateorie = 4;
            form.Show();
            this.Close();
        }
    }
}
