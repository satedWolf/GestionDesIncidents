﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionDesIncidents
{
    public partial class FormGestion : Form
    {
        public FormGestion()
        {
            InitializeComponent();
        }

        private void FormGestion_Load(object sender, EventArgs e)
        {
            var db = new DB();
            var cat = db.GetCategories();
            comboCategories.Items.AddRange(cat.Select(x => x.ID + "-" + x.Nom).ToArray());
        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
            var db = new DB();
            var sousCat = new SousCategorie();
            sousCat.Categorie_id = Int32.Parse( comboCategories.SelectedItem.ToString().Split('-')[0]);
            sousCat.Nom = textNom.Text;
            db.InsertSousCategories(sousCat);
            this.Close();
        }

        private void Retour_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
