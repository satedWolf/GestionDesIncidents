﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionDesIncidents
{
    class Incident
    {
        public int ID { get; set; }
        public int Categorie_Id { get; set; }
        public int Sous_Categorie_Id { get; set; }
        public int Rx { get; set; }
        public string Medicament { get; set; }
        public string Description { get; set; }
        public string Initiale_PHM { get; set; }
        public DateTime DateSignalement { get; set; }
    }
}
