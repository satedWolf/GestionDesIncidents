﻿namespace GestionDesIncidents
{
    partial class FormIncident
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.btnRetour = new System.Windows.Forms.Button();
            this.cmbSousCategories = new System.Windows.Forms.ComboBox();
            this.textRx = new System.Windows.Forms.TextBox();
            this.textMedicament = new System.Windows.Forms.TextBox();
            this.textDescription = new System.Windows.Forms.TextBox();
            this.textInitiale = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(185, 199);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(170, 56);
            this.button1.TabIndex = 5;
            this.button1.Text = "Enregistrer";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnRetour
            // 
            this.btnRetour.Location = new System.Drawing.Point(9, 199);
            this.btnRetour.Name = "btnRetour";
            this.btnRetour.Size = new System.Drawing.Size(170, 56);
            this.btnRetour.TabIndex = 6;
            this.btnRetour.Text = "Retour";
            this.btnRetour.UseVisualStyleBackColor = true;
            this.btnRetour.Click += new System.EventHandler(this.btnRetour_Click);
            // 
            // cmbSousCategories
            // 
            this.cmbSousCategories.FormattingEnabled = true;
            this.cmbSousCategories.Location = new System.Drawing.Point(93, 69);
            this.cmbSousCategories.Name = "cmbSousCategories";
            this.cmbSousCategories.Size = new System.Drawing.Size(263, 21);
            this.cmbSousCategories.TabIndex = 0;
            // 
            // textRx
            // 
            this.textRx.Location = new System.Drawing.Point(93, 95);
            this.textRx.Name = "textRx";
            this.textRx.Size = new System.Drawing.Size(263, 20);
            this.textRx.TabIndex = 1;
            // 
            // textMedicament
            // 
            this.textMedicament.Location = new System.Drawing.Point(93, 120);
            this.textMedicament.Name = "textMedicament";
            this.textMedicament.Size = new System.Drawing.Size(263, 20);
            this.textMedicament.TabIndex = 2;
            // 
            // textDescription
            // 
            this.textDescription.Location = new System.Drawing.Point(93, 146);
            this.textDescription.Name = "textDescription";
            this.textDescription.Size = new System.Drawing.Size(263, 20);
            this.textDescription.TabIndex = 3;
            // 
            // textInitiale
            // 
            this.textInitiale.Location = new System.Drawing.Point(93, 172);
            this.textInitiale.MaxLength = 4;
            this.textInitiale.Name = "textInitiale";
            this.textInitiale.Size = new System.Drawing.Size(263, 20);
            this.textInitiale.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 71);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Sous Catégorie";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 99);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Rx";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 174);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Initiale";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 150);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Description";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 122);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Medicament";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(1, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(354, 44);
            this.label3.TabIndex = 14;
            this.label3.Text = "Signaler un incident";
            // 
            // FormIncident
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(362, 260);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textInitiale);
            this.Controls.Add(this.textDescription);
            this.Controls.Add(this.textMedicament);
            this.Controls.Add(this.textRx);
            this.Controls.Add(this.cmbSousCategories);
            this.Controls.Add(this.btnRetour);
            this.Controls.Add(this.button1);
            this.Name = "FormIncident";
            this.Text = "Incident";
            this.Load += new System.EventHandler(this.FormIncident_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnRetour;
        private System.Windows.Forms.ComboBox cmbSousCategories;
        private System.Windows.Forms.TextBox textRx;
        private System.Windows.Forms.TextBox textMedicament;
        private System.Windows.Forms.TextBox textDescription;
        private System.Windows.Forms.TextBox textInitiale;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
    }
}