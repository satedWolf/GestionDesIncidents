﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionDesIncidents
{
    class SousCategorie
    {
        public int ID { get; set; }
        public string Nom { get; set; }
        public int Categorie_id { get; set; }
    }
}
