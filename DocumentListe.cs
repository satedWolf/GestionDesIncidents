﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Diagnostics;

namespace GestionDesIncidents
{
    class DocumentListe
    {

        public void GenererRapportListe(String date )
        {
            try
            {
                var db = new DB();
                var incidents = db.GetIncidents().Where(x => x.DateSignalement.Month == Int32.Parse(date.Split('-')[0]));
                var souscategories = db.GetSousCategories();
                var categories = db.GetCategories();
                Document document = new Document(PageSize.LETTER, 10f, 10f, 35f, 35f);
                PdfWriter.GetInstance(document, (Stream)new FileStream("Rapport\\" + date + ".pdf", FileMode.Create));
                Paragraph paragraph1 = new Paragraph("Rapport des Incidents en " + date + "\n\n");
                paragraph1.Font.Size = 18;
                document.Open();
                document.Add(paragraph1);
                foreach (var cat in categories)
                {
                    Paragraph categorie = new Paragraph(cat.Nom + " : " + incidents.Where(x => x.Categorie_Id == cat.ID).Count().ToString() + "\n");
                    foreach (var item in souscategories.Where(x => x.Categorie_id == cat.ID))
                    {
                        categorie.Add(item.Nom + " : " + incidents.Where(x => x.Sous_Categorie_Id == item.ID).Count().ToString() + "\n");
                    }
                    if (cat.ID != 4)
                    {
                        categorie.Add("\n");
                    }

                    document.Add(categorie);
                }
                document.Close();
                Process.Start("Rapport\\" + date + ".pdf");
            }
            catch (Exception)
            {

            }
           
        }



        public void GenererRapportDetaille(String date)
        {
            try
            {

                var db = new DB();
                var incidents = db.GetIncidents().Where(x => x.DateSignalement.Month == Int32.Parse(date.Split('-')[0])).OrderBy(x => x.DateSignalement);
                var souscategories = db.GetSousCategories();
                var categories = db.GetCategories();
                Document document = new Document(PageSize.LETTER, 10f, 10f, 35f, 35f);
                PdfWriter.GetInstance(document, (Stream)new FileStream("Listes\\" + date + ".pdf", FileMode.Create));
                Paragraph paragraph1 = new Paragraph("Liste des Incidents en " + date + "\n Nombre total : " + incidents.Count() +"\n\n");
                Paragraph paragraphe = new Paragraph();
                paragraph1.Font.Size = 18;
                document.Open();
                document.Add(paragraph1);

                foreach (var item in incidents)
                {
                    paragraphe.Add(item.DateSignalement.ToShortDateString() + "   " + categories.Where(x => x.ID == item.Categorie_Id).Single().Nom + "   " + souscategories.Where(x => x.ID == item.Sous_Categorie_Id).Single().Nom + "   " + (item.Rx == 0 ? "": item.Rx.ToString()) + "   " + item.Medicament + "   " + item.Description + "   " + item.Initiale_PHM + "\n");
                }
                document.Add(paragraphe);
                document.Close();
                Process.Start("Listes\\" + date + ".pdf");

            }
            catch (Exception) { }
        }

    }
}
