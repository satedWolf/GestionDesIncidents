﻿namespace GestionDesIncidents
{
    partial class RapportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGenere = new System.Windows.Forms.Button();
            this.DTPMonth = new System.Windows.Forms.DateTimePicker();
            this.btnRetour = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnGenere
            // 
            this.btnGenere.Location = new System.Drawing.Point(12, 38);
            this.btnGenere.Name = "btnGenere";
            this.btnGenere.Size = new System.Drawing.Size(163, 53);
            this.btnGenere.TabIndex = 2;
            this.btnGenere.Text = "Generer";
            this.btnGenere.UseVisualStyleBackColor = true;
            this.btnGenere.Click += new System.EventHandler(this.btnGenere_Click);
            // 
            // DTPMonth
            // 
            this.DTPMonth.Location = new System.Drawing.Point(12, 12);
            this.DTPMonth.Name = "DTPMonth";
            this.DTPMonth.Size = new System.Drawing.Size(332, 20);
            this.DTPMonth.TabIndex = 3;
            // 
            // btnRetour
            // 
            this.btnRetour.Location = new System.Drawing.Point(181, 38);
            this.btnRetour.Name = "btnRetour";
            this.btnRetour.Size = new System.Drawing.Size(163, 53);
            this.btnRetour.TabIndex = 4;
            this.btnRetour.Text = "Retour";
            this.btnRetour.UseVisualStyleBackColor = true;
            this.btnRetour.Click += new System.EventHandler(this.btnRetour_Click);
            // 
            // RapportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(352, 99);
            this.Controls.Add(this.btnRetour);
            this.Controls.Add(this.DTPMonth);
            this.Controls.Add(this.btnGenere);
            this.Name = "RapportForm";
            this.Text = "Generer un rapport";
            this.Load += new System.EventHandler(this.RapportForm_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnGenere;
        private System.Windows.Forms.DateTimePicker DTPMonth;
        private System.Windows.Forms.Button btnRetour;
    }
}