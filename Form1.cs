﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;

namespace GestionDesIncidents
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private int IdCategorie;
        private void Form1_Load(object sender, EventArgs e)
        {
           
        }

        private void BtnSignaler_Click(object sender, EventArgs e)
        {
            FormSignaler form = new FormSignaler();
            form.Show();
        }

        private void BtnQuitter_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnGenerer_Click(object sender, EventArgs e)
        {
            RapportForm form = new RapportForm();
            form.isDetaille = false;
            form.Show();
        }

        private void btnRapportDetaille_Click(object sender, EventArgs e)
        {
            RapportForm form = new RapportForm();
            form.isDetaille = true;
            form.Show();
        }

        private void btnGestion_Click(object sender, EventArgs e)
        {
            FormGestion form = new FormGestion();
            form.Show();
        }
    }
}
