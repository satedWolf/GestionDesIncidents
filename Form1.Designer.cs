﻿namespace GestionDesIncidents
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnGenerer = new System.Windows.Forms.Button();
            this.BtnQuitter = new System.Windows.Forms.Button();
            this.BtnSignaler = new System.Windows.Forms.Button();
            this.btnRapportDetaille = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnGestion = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BtnGenerer
            // 
            this.BtnGenerer.Location = new System.Drawing.Point(7, 175);
            this.BtnGenerer.Margin = new System.Windows.Forms.Padding(2);
            this.BtnGenerer.Name = "BtnGenerer";
            this.BtnGenerer.Size = new System.Drawing.Size(342, 72);
            this.BtnGenerer.TabIndex = 0;
            this.BtnGenerer.Text = "Rapport signalement";
            this.BtnGenerer.UseVisualStyleBackColor = true;
            this.BtnGenerer.Click += new System.EventHandler(this.BtnGenerer_Click);
            // 
            // BtnQuitter
            // 
            this.BtnQuitter.Location = new System.Drawing.Point(5, 403);
            this.BtnQuitter.Margin = new System.Windows.Forms.Padding(2);
            this.BtnQuitter.Name = "BtnQuitter";
            this.BtnQuitter.Size = new System.Drawing.Size(342, 74);
            this.BtnQuitter.TabIndex = 1;
            this.BtnQuitter.Text = "Quitter";
            this.BtnQuitter.UseVisualStyleBackColor = true;
            this.BtnQuitter.Click += new System.EventHandler(this.BtnQuitter_Click);
            // 
            // BtnSignaler
            // 
            this.BtnSignaler.Location = new System.Drawing.Point(5, 99);
            this.BtnSignaler.Margin = new System.Windows.Forms.Padding(2);
            this.BtnSignaler.Name = "BtnSignaler";
            this.BtnSignaler.Size = new System.Drawing.Size(342, 72);
            this.BtnSignaler.TabIndex = 2;
            this.BtnSignaler.Text = "Signaler un incident";
            this.BtnSignaler.UseVisualStyleBackColor = true;
            this.BtnSignaler.Click += new System.EventHandler(this.BtnSignaler_Click);
            // 
            // btnRapportDetaille
            // 
            this.btnRapportDetaille.Location = new System.Drawing.Point(7, 249);
            this.btnRapportDetaille.Margin = new System.Windows.Forms.Padding(2);
            this.btnRapportDetaille.Name = "btnRapportDetaille";
            this.btnRapportDetaille.Size = new System.Drawing.Size(342, 72);
            this.btnRapportDetaille.TabIndex = 3;
            this.btnRapportDetaille.Text = "Rapport détaillé";
            this.btnRapportDetaille.UseVisualStyleBackColor = true;
            this.btnRapportDetaille.Click += new System.EventHandler(this.btnRapportDetaille_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(344, 39);
            this.label1.TabIndex = 4;
            this.label1.Text = "Gestion des Incidents";
            // 
            // btnGestion
            // 
            this.btnGestion.Location = new System.Drawing.Point(5, 325);
            this.btnGestion.Margin = new System.Windows.Forms.Padding(2);
            this.btnGestion.Name = "btnGestion";
            this.btnGestion.Size = new System.Drawing.Size(342, 74);
            this.btnGestion.TabIndex = 5;
            this.btnGestion.Text = "Gestion";
            this.btnGestion.UseVisualStyleBackColor = true;
            this.btnGestion.Click += new System.EventHandler(this.btnGestion_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(353, 488);
            this.Controls.Add(this.btnGestion);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnRapportDetaille);
            this.Controls.Add(this.BtnSignaler);
            this.Controls.Add(this.BtnQuitter);
            this.Controls.Add(this.BtnGenerer);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Gestion des Incidents";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnGenerer;
        private System.Windows.Forms.Button BtnQuitter;
        private System.Windows.Forms.Button BtnSignaler;
        private System.Windows.Forms.Button btnRapportDetaille;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnGestion;
    }
}

