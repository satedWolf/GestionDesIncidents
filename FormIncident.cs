﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionDesIncidents
{
    public partial class FormIncident : Form
    {
        public int Cateorie { get; set; }
        public FormIncident()
        {
            InitializeComponent();
        }

        private void btnRetour_Click(object sender, EventArgs e)
        {
            FormSignaler Form = new FormSignaler();
            Form.Show();
            this.Close();
        }

        private void FormIncident_Load(object sender, EventArgs e)
        {
            var db = new DB();
            var sousCategories = db.GetSousCategories().Where(x => x.Categorie_id == this.Cateorie).ToList();
            cmbSousCategories.Items.AddRange(sousCategories.Select(x => x.ID + "-" + x.Nom).ToArray());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var incident = new Incident();
            var db = new DB();
            bool fini = true;
            try
            {
                incident.Categorie_Id = this.Cateorie;
                incident.Sous_Categorie_Id = Int32.Parse(cmbSousCategories.SelectedItem.ToString().Split('-')[0]);
                if (textRx.Text == "")
                {
                    incident.Rx = 0;
                }else
                {
                    incident.Rx = Int32.Parse(textRx.Text);
                }
                
                incident.Medicament = textMedicament.Text;
                incident.Initiale_PHM = textInitiale.Text;
                incident.Description = textDescription.Text;
                incident.DateSignalement = DateTime.Now;
                db.InsertIncidents(incident);
               
            }
            catch (Exception)
            {
                MessageBox.Show("Un de vos champs contient une valeur invalide !", "Un de vos champs contient une valeur invalide !",MessageBoxButtons.OK, MessageBoxIcon.Error);
                fini = false;
            }

            if (fini)
            {
                FormSignaler from = new FormSignaler();
                from.Show();
                this.Close();
            }

        }
    }
}
