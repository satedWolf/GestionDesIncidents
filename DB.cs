﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OleDb;

namespace GestionDesIncidents
{

    
    class DB
    {
        
        //Retourne tout les categories de la DB en list.
        public  List<Categorie> GetCategories()
        {
            var list = new List<Categorie>(); 
            string constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source =DB.accdb";
            using (OleDbConnection con = new OleDbConnection(constr))
            {
                using (OleDbCommand cmd = new OleDbCommand("SELECT * FROM Categories"))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (OleDbDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            var tempCat = new Categorie();
                            tempCat.ID = Int32.Parse( sdr["ID"].ToString());
                            tempCat.Nom = sdr["Nom"].ToString();
                            list.Add(tempCat);
                        }
                    }
                    con.Close();
                }
            }
            return list;
        }
        public List<SousCategorie> GetSousCategories()
        {
            var list = new List<SousCategorie>();
            string constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source =DB.accdb";
            using (OleDbConnection con = new OleDbConnection(constr))
            {
                using (OleDbCommand cmd = new OleDbCommand("SELECT * FROM Sous_Categories"))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (OleDbDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            var tempSousCat = new SousCategorie();
                            tempSousCat.ID = Int32.Parse(sdr["ID"].ToString());
                            tempSousCat.Categorie_id = Int32.Parse(sdr["Categorie_ID"].ToString());
                            tempSousCat.Nom = sdr["Nom"].ToString();
                            list.Add(tempSousCat);
                        }
                    }
                    con.Close();
                }
            }
            return list;
        }
        public List<Incident> GetIncidents()
        {
            var list = new List<Incident>();
            string constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source =DB.accdb";
            using (OleDbConnection con = new OleDbConnection(constr))
            {
                using (OleDbCommand cmd = new OleDbCommand("SELECT * FROM Incidents"))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (OleDbDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            var tempIncident = new Incident();
                            tempIncident.ID = Int32.Parse(sdr["ID"].ToString());
                            tempIncident.Categorie_Id = Int32.Parse(sdr["Categorie_ID"].ToString());
                            tempIncident.Sous_Categorie_Id  = Int32.Parse(sdr["Sous_Categorie_Id"].ToString());
                            tempIncident.Rx = Int32.Parse(sdr["Rx"].ToString());
                            tempIncident.Medicament = sdr["Medicament"].ToString();
                            tempIncident.Description = sdr["Description"].ToString();
                            tempIncident.Initiale_PHM = sdr["Initiale_PHM"].ToString();
                            tempIncident.DateSignalement =  DateTime.Parse( sdr["DateSignalement"].ToString());
                            list.Add(tempIncident);
                        }
                    }
                    con.Close();
                }
            }
            return list;
        }
        public void InsertIncidents(Incident incident)
        {
            string constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source =DB.accdb";
            using (OleDbConnection con = new OleDbConnection(constr))
            {
                using (OleDbCommand cmd = new OleDbCommand("insert into Incidents (Categorie_Id,Sous_Categorie_Id,Rx,Medicament,Description,Initiale_PHM,DateSignalement) values (" + incident.Categorie_Id + ", " + incident.Sous_Categorie_Id + ", " + incident.Rx + ", '" + incident.Medicament + "', '" + incident.Description + "', '" + incident.Initiale_PHM + "', '" + Convert.ToDateTime(incident.DateSignalement.ToString()) + "')"))
                {
                     
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();

                    con.Close();
                    
                }
            }
        }
        public void InsertSousCategories(SousCategorie item)
        {
            string constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source =DB.accdb";
            using (OleDbConnection con = new OleDbConnection(constr))
            {
                using (OleDbCommand cmd = new OleDbCommand("insert into Sous_Categories (Categorie_Id,Nom) values (" + item.Categorie_id + ", '" + item.Nom + "')"))
                {

                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();

                    con.Close();

                }
            }
        }

    }
}
