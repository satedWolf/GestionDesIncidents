﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionDesIncidents
{
    public partial class RapportForm : Form
    {
        public bool isDetaille { get; set; }
        public RapportForm()
        {
            InitializeComponent();
        }

        private void RapportForm_Load(object sender, EventArgs e)
        {
            DTPMonth.Format = DateTimePickerFormat.Custom;
            DTPMonth.CustomFormat = "MM/yyyy";
        }

        private void btnRetour_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGenere_Click(object sender, EventArgs e)
        {
            var document = new DocumentListe();
            if (isDetaille)
            {
                document.GenererRapportDetaille(DTPMonth.Value.Month.ToString() + "-" + DTPMonth.Value.Year.ToString());
            }
            else
            {
                document.GenererRapportListe(DTPMonth.Value.Month.ToString() + "-" + DTPMonth.Value.Year.ToString());
            }
           
        }
    }
}
